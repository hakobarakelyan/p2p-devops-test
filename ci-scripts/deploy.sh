#!/bin/bash
set -euo pipefail errexit

export KUBE_NAMESPACE=${CI_ENVIRONMENT_NAME}
export RELEASE_NAME=${CI_PROJECT_NAME}

kubectl config use-context $KUBE_CONTEXT;

helm upgrade --install --atomic ${RELEASE_NAME} \
  --namespace=${KUBE_NAMESPACE} ${HELM_CHART_DIR} \
  --set image.tag=${IMAGE_TAG} \
  --values ${HELM_VALUES_DIR}/${CI_ENVIRONMENT_NAME}.values.yaml
