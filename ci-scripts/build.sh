#!/usr/bin/env bash

set -euo pipefail

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker build --progress auto --tag ${IMAGE}:${IMAGE_TAG} .

docker push ${IMAGE}:${IMAGE_TAG}
