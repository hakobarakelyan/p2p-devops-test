FROM golang:alpine AS builder

RUN apk add --no-cache tzdata

RUN apk --update add --no-cache ca-certificates

WORKDIR /app

COPY . .

RUN go mod download

RUN go mod verify

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /p2p-devops-test .

FROM scratch

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=builder /etc/passwd /etc/passwd

COPY --from=builder /etc/group /etc/group

COPY --from=builder /p2p-devops-test .

EXPOSE 3000

CMD [ "/p2p-devops-test" ]
