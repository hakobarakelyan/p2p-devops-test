# Hello World Go App in 21st Century

## Building and storing docker image in (private) registry

```bash
docker login
docker build --tag p2p-devops-test .
docker push p2p-devops-test
```
